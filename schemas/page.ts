// schemas/brand.ts
export default {
    name: 'page',
    type: 'document',
    title: 'Page',
    fields: [
      {
        name: 'title',
        type: 'string',
        title: 'Title',
      },
      {
        name: 'subtitle',
        type: 'string',
        title: 'Subtitle',
      },
      {
        name: 'slug',
        title: 'Slug',
        type: 'slug',
        options: {
          source: 'title',
          maxLength: 96,
        },
      },
      {
        name: 'mainImage',
        title: 'Main Image',
        type: 'image',
        options: {
          hotspot: true,
        },
      },
      {
        name: 'description',
        type: 'string',
        title: 'Description',
      },

    ],
    preview: {
      select: {
        title: 'title',
        media: 'mainImage'
      },
    },
  }