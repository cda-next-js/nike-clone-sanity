// schemas/size.js
export default {
    name: 'category',
    type: 'document',
    title: 'Category',
    fields: [
      {
        name: 'name',
        type: 'string',
        title: 'name'
      }
    ],
    preview: {
        select: {
          title: 'name',
         
        },
        
      },
  }