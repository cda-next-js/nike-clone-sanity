// schemas/size.js
export default {
  name: 'variant',
  type: 'document',
  title: 'Variant',
  fields: [
    {
      title: 'Product',
      name: 'product',
      type: 'reference',
      to: [{type: 'product'}],
    },
    {
      title: 'Color',
      name: 'color',
      type: 'reference',
      to: [{type: 'color'}],
    },
    {
      name: 'mainImage',
      title: 'Main Image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'images',
      title: 'Images',
      type: 'array',
      of: [
        {
          type: 'image',
          options: {
            hotspot: true,
          },
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'product.name',
      media: 'mainImage',
    },
  },
}